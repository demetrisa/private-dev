#!/bin/bash
tags=${1:-dev} 
echo 'Build tags='$tags
ARTIFACTS_PATH="$(pwd)/"
curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
cd ${IMPORT_PATH}
dep ensure
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -tags=$tags -installsuffix cgo -o hello
